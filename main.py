import os
import sys

# --------------------- ALGORYTM ---------------------

EPSILON = 1e-7

class Matrix:
    rows: int
    cols: int

    def __init__(self, lst):
        self._lst = lst
        self.rows = len(lst)
        self.cols = len(lst[0])

    def __getitem__(self, item):
        return self._lst[item]

    def __setitem__(self, key, value):
        self._lst[key] = value


def gauss_basic(A):
    for s in range(A.rows):
        resolve_step(A, s)

    return solve(A)


def gauss_fullmax(A):
    col_map = [c for c in range(A.cols)]

    for s in range(A.rows):
        m = fullmax(A, s, A.rows)
        if m[0] != s:
            swap_rows(A, s, m[0])
        if m[1] != s:
            swap_columns(A, s, m[1], col_map)
        resolve_step(A, s)

    x = solve(A)
    xi = [0] * (A.cols - 1)
    for i in range(A.cols - 1):
        xi[col_map[i]] = x[i]
    return xi


def gauss_colmax(A):
    for s in range(A.rows):
        m = colmax(A, s, s, A.rows)
        if m != s:
            swap_rows(A, m, s)
        resolve_step(A, s)

    return solve(A)


class DomainError(Exception):
    pass


def fullmax(A, start, stop):
    top_i = (start, start)
    for r in range(start, stop):
        for c in range(start, stop):
            if abs(A[r][c]) > abs(A[top_i[0]][top_i[1]]):
                top_i = (r, c)
    return top_i


def colmax(A, c, start, stop):
    top_i = start
    for r in range(start + 1, stop):
        if abs(A[r][c]) > abs(A[top_i][c]):
            top_i = r
    return top_i


def resolve_step(A, step):
    p = A[step][step]
    if not rel_zero(p):
        for i in range(step + 1, A.rows):
            w = A[i][step] / p
            if w != 0:
                A[i] = [a - w * f for a, f in zip(A[i], A[step])]
    else:
        raise DomainError


def swap_rows(A, row1, row2):
    tmp = A[row1]
    A[row1] = A[row2]
    A[row2] = tmp


def swap_columns(A, col1, col2, col_map):
    col_map[col1], col_map[col2] = col_map[col2], col_map[col1]
    for i in range(0, A.rows):
        tmp = A[i][col1]
        A[i][col1] = A[i][col2]
        A[i][col2] = tmp


def solve(A):
    x = [0] * A.rows
    for i in reversed(range(0, A.rows)):
        x[i] = (A[i][-1] - sum([a * x for (a, x) in zip(A[i][i + 1:-1], x[i + 1:])])) / A[i][i]

    return x


def rel_zero(v):
    return abs(v) < EPSILON


# --------------------- INTERFEJS ---------------------


DEFAULT_MATRICES = {
    gauss_basic: [
        [2.25, -2.5, 4, -5.25, -1],
        [-3, -7.5, 6.5, 0, 17],
        [-6.25, -12.5, 0.25, 5.25, 24.25],
        [9, 10, 7, -21, -33]
    ],
    gauss_colmax: [
        [1, 1, 0, -3, 1],
        [1, 4, -1, -4, -2],
        [0.5, 0.5, -3, -5.5, 1.5],
        [1.5, 3, -5, -9, -0.5]
    ],
    gauss_fullmax: [
        [1, 2, -1, 2, 0],
        [1, 0, -2, 4, 4],
        [0, -3, 1.5, 7, 0],
        [0, -1, 1, 6, -1]
    ]
}


DIALOG_MATRIX = """Domyślne dane wejściowe:
{}
Czy chcesz podać własne dane?
    1. Tak
    2. Nie (test dla domyślnych danych wejściowych)\n"""
DIALOG_METHOD = """Wybierz metodę eliminacji Gaussa:
    1. Podstawowa
    2. Z wyborem elementu maksymalnego w kolumnie
    3. Z pełnym wyborem elementu maksymalnego\n"""
DIALOG_SOLVE = """Rozwiązanie układu równań reprezentowanego przez macierz rozszerzoną
{}
za pomocą metody {}:
    {}
"""
PROMPT_MATRIX_SIZE = "Podaj szerokość macierzy kwadratowej: "
PROMPT_MATRIX = "Wprowadź elementy macierzy rozszerzonej o wektor wyrazów wolnych:\n"
lang = {
    gauss_basic: 'podstawowej',
    gauss_colmax: 'z wyborem elementu maksymalnego w kolumnie',
    gauss_fullmax: 'z pełnym wyborem elementu maksymalnego'
}


class MatrixReader:
    pos = (0, 0)
    size: (int, int)

    def __init__(self, size):
        self.size = size
        self.mx = []
        for i in range(0, size[0]):
            r = []
            for j in range(0, size[1]):
                r.append(None)
            self.mx.append(r)

    def show(self):
        for idr, row in enumerate(self.mx):
            for idc, elem in enumerate(row):
                if (idr, idc) == self.pos:
                    print(cell('_'), end='')
                else:
                    print(cell(elem if elem is not None else '-'), end='')
            print()

    def prompt_next(self):
        self.mx[self.pos[0]][self.pos[1]] = float(input(f'\n[{self.pos[0]}, {self.pos[1]}] = '))
        if self.pos[1] == self.size[1] - 1:
            self.pos = (self.pos[0] + 1, 0)
        else:
            self.pos = (self.pos[0], self.pos[1] + 1)

    def filled(self):
        return self.pos[0] > self.size[0] - 1


def prompt_method():
    inp = input(DIALOG_METHOD)
    choice = int(inp)
    if choice not in [1, 2, 3]:
        on_invalid_input()
    else:
        return [gauss_basic, gauss_colmax, gauss_fullmax][choice - 1]


def prompt_custom_matrix(size):
    print(PROMPT_MATRIX)
    mr = MatrixReader((size, size + 1))
    while not mr.filled():
        cls()
        mr.show()
        mr.prompt_next()
    return mr.mx


def prompt_matrix(gauss):
    inp = input(DIALOG_MATRIX.format(matrix_string(DEFAULT_MATRICES[gauss])))
    choice = int(inp)
    if choice not in [1, 2]:
        on_invalid_input()
    if choice == 1:
        return prompt_custom_matrix(int(input(PROMPT_MATRIX_SIZE)))
    else:
        return DEFAULT_MATRICES[gauss]


def on_invalid_input():
    print("Nieprawidłowy wybór", file=sys.stderr)
    exit(1)


def on_domain_error():
    print("Algorytm niewykonalny dla wprowadzonych danych", file=sys.stderr)
    exit(1)


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def matrix_string(mx):
    s = ""
    for idr, row in enumerate(mx):
        for idc, elem in enumerate(row):
            s += cell(elem if elem is not None else '-')
        s += '\n'
    return s[:-1]


def cell(content):
    if type(content) in (float, int):
        return f'{"%.2f" % content: ^8}'
    else:
        return f'{content: ^8}'


def round_vector(vector):
    return [round(x, 7) for x in vector]


if __name__ == '__main__':
    method = prompt_method()
    matrix = prompt_matrix(method)
    matrix_str = matrix_string(matrix)
    try:
        result = method(Matrix(matrix.copy()))
    except DomainError:
        on_domain_error()

    print(DIALOG_SOLVE.format(matrix_str, lang[method], round_vector(result)))
