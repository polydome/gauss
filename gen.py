import random

import numpy as np

from main import EPSILON


def random_num():
    return (random.randrange(0, 20000) - 10000) / 100


def random_matrix(variables, eqs):
    return [[random_num() for _ in range(variables + 1)] for _ in range(eqs)]


def assert_solved(matrix, result):
    for eq in matrix:
        if not almost_equal(sum([var * p for var, p in zip(eq[:-1], result)]), eq[-1]):
            raise AssertionError("Failed for A = {}\nX = {}".format(np.array(matrix), result))


def almost_equal(a, b):
    return abs(a - b) < EPSILON
